package com.cloudwave.trailends.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.cloudwave.trailends.domain.UserInfo;
import com.cloudwave.trailends.mapper.UserInfoMapper;
import com.cloudwave.trailends.mapper.UserMapper;
import com.cloudwave.trailends.service.UserInfoService;

/**
 * @description 
 * @author wangwenlong
 * @date 2014年1月4日
 * TODO
 */

@Service
public class UserInfoServiceImpl implements UserInfoService {
	
	@Resource
	private UserMapper userMapper;
	@Resource
	private UserInfoMapper userInfoMapper;

	@Override
	public void save(UserInfo userInfo) {
		this.userInfoMapper.insert(userInfo);
	}

	@Override
	public UserInfo findByUserId(Long userId) {
		UserInfo userInfo = userInfoMapper.findByUserId(userId);
		if ( userInfo == null ) {
			return new UserInfo();
		} else {
			return userInfo;
		}
	}

	@Override
	public void update(UserInfo userInfo) {
		userInfoMapper.update(userInfo);
	}
	
	
}
