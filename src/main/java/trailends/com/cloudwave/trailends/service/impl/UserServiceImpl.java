package com.cloudwave.trailends.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.cloudwave.trailends.beans.UserHomeBean;
import com.cloudwave.trailends.domain.User;
import com.cloudwave.trailends.entity.UserInfoEntity;
import com.cloudwave.trailends.mapper.LinePointMapper;
import com.cloudwave.trailends.mapper.RidingLineMapper;
import com.cloudwave.trailends.mapper.UserInfoMapper;
import com.cloudwave.trailends.mapper.UserMapper;
import com.cloudwave.trailends.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Resource
	private UserMapper userMapper;
	@Resource
	private UserInfoMapper userInfoMapper;
	@Resource
	private RidingLineMapper ridingLineMapper;
	@Resource
	private LinePointMapper linePointMapper;
	
	
	@Override
	public User get(long id) {
		return this.userMapper.get(id);
	}
	
	@Override
	public void save(User u) throws Exception {
		this.userMapper.insert(u);
	}

	@Override
	public User findByUserName(String username) {
		return this.userMapper.findByUserName(username);
	}

	@Override
	public User getByAccountOrEmail(String accountOrEmail) {
		return this.userMapper.findByAccountOrEmail(accountOrEmail);
	}

	@Override
	public boolean checkEmail(String email) {
		int sum = this.userMapper.countByEmail(email);
		if (sum > 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean checkUsername(String username) {
		int count = this.userMapper.countByUsername(username);
		return count > 0 ? true : false;
	}

	@Override
	public List<User> findByIds(List<Long> ids) {
		return this.userMapper.findByIds(ids);
	}

	@Override
	public UserHomeBean loadUserHome(Long id) {
		return userMapper.loadUserHome(id);
	}

	@Override
	public User getByEmail(String email) {
		return userMapper.getByEmail(email);
	}

	@Override
	public boolean checkNameAndEmail(String username, String email) {
		int count = userMapper.countByNameAndEmail(username, email);
		return count > 0 ? true : false;
	}

	@Override
	public User findByToken(String token) {
		return userMapper.findByToken(token);
	}

	@Override
	public Long findIdByToken(String token) {
		return userMapper.findIdByToken(token);
	}

	
	@Override
	public boolean isExistsAccount(String account) {
		int count = this.userMapper.countByAccount(account);
		return count > 0 ? true : false;
	}

	@Override
	public void updateAvatar(Long userId, String url) {
		userMapper.updateAvatar(userId, url);
	}

	@Override
	public void updateToken(Long userId, String token) {
		userMapper.updateToken(userId, token);
	}

	@Override
	public void updateUserStatData(Long userId) {
		Map<String, Object> userStatMap = ridingLineMapper.statByUserId(userId);
		
		userInfoMapper.updateUserStat(userStatMap);
	}

	@Override
	public User findByOpenId(String openid) {
		return userMapper.findByOpenId(openid);
	}

	@Override
	public void saveOpenUser(User insUser) {
		try {
			userMapper.insert(insUser);
		} catch (DuplicateKeyException e) {
			String newUserName = insUser.getUsername()+System.currentTimeMillis();
			insUser.setUsername(newUserName);
			userMapper.insert(insUser);
		}
	}

	@Override
	public void update(UserInfoEntity userInfo) {
		User user = new User(userInfo);
		userMapper.updateBaseInfo(user);
	}

}
