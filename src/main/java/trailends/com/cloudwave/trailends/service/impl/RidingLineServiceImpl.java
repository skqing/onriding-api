package com.cloudwave.trailends.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import com.cloudwave.trailends.domain.LinePoint;
import com.cloudwave.trailends.domain.RidingLine;
import com.cloudwave.trailends.mapper.LinePointMapper;
import com.cloudwave.trailends.mapper.RidingLineMapper;
import com.cloudwave.trailends.service.RidingLineService;

/**
 * @description 
 * @author wangwenlong
 * @date 2014年7月26日
 * TODO
 */

@Service
public class RidingLineServiceImpl implements RidingLineService {

	@Resource
	private RidingLineMapper ridingLineMapper;
	@Resource
	private LinePointMapper linePointMapper;
	
	@Override
	public void save(RidingLine ridingLine) {
		ridingLineMapper.insert(ridingLine);
	}

	@Override
	public List<RidingLine> findNotInIds(String rlIds) {
		return ridingLineMapper.findNotInIds(rlIds);
	}

	@Override
	public List<RidingLine> findBy(Map<String, Object> params) {
		return ridingLineMapper.findBy(params);
	}

	@Override
	public List<RidingLine> findAll(Map<String, Object> params) {
		return ridingLineMapper.findAll(params);
	}

	@Override
	public void saveLinesAndPoints(List<RidingLine> rList) {
		for (RidingLine rl : rList) {
			ridingLineMapper.insert(rl);
			List<LinePoint> lpList = rl.getLpList();
			if (CollectionUtils.isEmpty(lpList)) {
				continue ;
			}
			for (LinePoint lp : lpList) {
				lp.setRidingLine(rl);
			}
			linePointMapper.insertAll(lpList);
		}
	}

	@Override
	public void saveLineAndPoints(RidingLine ridingLine, List<LinePoint> lpList) {
		ridingLineMapper.insert(ridingLine);
		for (LinePoint lp : lpList) {
			lp.setRidingLine(ridingLine);
		}
		linePointMapper.insertAll(lpList);
	}

	@Override
	public void saveWithsPoints(RidingLine rl) {
		ridingLineMapper.insert(rl);
		List<LinePoint> lpList = rl.getLpList();
		if (CollectionUtils.isEmpty(lpList)) {
			return ;
		}
		for (LinePoint lp : rl.getLpList()) {
			lp.setRidingLine(rl);
			lp.setId(null);
		}
		linePointMapper.insertAll(lpList);
	}

	@Override
	public RidingLine findById(Long id) {
		return ridingLineMapper.findById(id);
	}

	@Override
	public void deleteById(Long id) {
		ridingLineMapper.findById(id);
		linePointMapper.deleteByLineId(id);
	}

	@Override
	public void deleteByIds(List<Long> idList) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("rlIdList", idList);
		
		linePointMapper.deleteByLineIds(params);
		ridingLineMapper.deleteByIds(params);
	}

	@Override
	public RidingLine findByLpId(Long lpId) {
		return ridingLineMapper.findByLpId(lpId);
	}

	@Override
	public RidingLine findFirst() {
		return ridingLineMapper.findFirst();
	}

	@Override
	public RidingLine findNextById(Long rlId) {
		RidingLine l = ridingLineMapper.findById(rlId);
		return ridingLineMapper.findNextByTime(l.getTimestamp());
	}

	@Override
	public void update(RidingLine rlTmp) {
		ridingLineMapper.update(rlTmp);
	}

}
