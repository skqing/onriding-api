package com.cloudwave.trailends.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.cloudwave.trailends.domain.Feedback;
import com.cloudwave.trailends.mapper.FeedbackMapper;
import com.cloudwave.trailends.service.FeedbackService;

/**
 * @author 龙雪
 * @email 569141948@qq.com
 * @date 2015-3-17 下午9:30:56
 * 
 */

@Service
public class FeedbackServiceImpl implements FeedbackService {

	@Resource
	private FeedbackMapper feedbackMapper;
	
	@Override
	public void save(Feedback fb) {
		feedbackMapper.insert(fb);
	}

}
