package com.cloudwave.trailends.mapper;

import com.cloudwave.trailends.domain.Feedback;

/**
 * @author 龙雪
 * @email 569141948@qq.com
 * @date 2015-3-17 下午9:30:05
 * 
 */

public interface FeedbackMapper {

	void insert(Feedback fb);

}
