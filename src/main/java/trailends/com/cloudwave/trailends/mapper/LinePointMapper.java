package com.cloudwave.trailends.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.cloudwave.base.entity.QueryEntity;
import com.cloudwave.trailends.domain.LinePoint;

/**
 * @description 
 * @author DolphinBoy
 * @email dolphinboyo@gmail.com
 * @date 2014-7-27 下午10:41:47
 * TODO
 */

public interface LinePointMapper {

	void insertAll(@Param("lpList") List<LinePoint> lpList);

	List<LinePoint> findNotInIds(String lpIds);

	List<LinePoint> findByLineId(String rlId);

	void deleteByLineId(Long rlId);

	Long countByLastTime(@Param("userId") Long userId, @Param("lastTimestamp") String lastTimestamp);

	List<LinePoint> findBy(QueryEntity query);

	void insert(LinePoint lp);

	void deleteByLineIds(Map<String, Object> params);

	long countByLineId(@Param("userId") Long userId, @Param("rlId") Long rlId
			, @Param("lastTimestamp") String lastTimestamp);

	long countByOtherLine(@Param("userId") Long userId, @Param("rlId") Long rlId);
	
	
}
