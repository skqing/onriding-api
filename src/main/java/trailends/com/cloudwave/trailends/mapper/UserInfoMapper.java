package com.cloudwave.trailends.mapper;

import java.util.Map;

import com.cloudwave.trailends.domain.UserInfo;

/**
 * @description 
 * @author 龙雪
 * @date 2014年1月4日
 * 
 */

public interface UserInfoMapper {

	void insert(UserInfo userInfo);

	void updateUserStat(Map<String, Object> userStatMap);

	UserInfo findByUserId(Long userId);

	void update(UserInfo userInfo);

}
