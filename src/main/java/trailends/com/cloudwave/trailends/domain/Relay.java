package com.cloudwave.trailends.domain;

import java.util.Date;

import com.cloudwave.base.domain.AppDomain;

/**
 * 转发  实体类
 * @author DolphinBoy
 * @date 2013-8-23
 */
public class Relay extends AppDomain {
	private static final long serialVersionUID = -4625028862529199355L;
	
	private TripRecord travelMessage;
	private String content;
	private User user;
	private Date sendTime;
	
	
	public TripRecord getTravelMessage() {
		return travelMessage;
	}
	public void setTravelMessage(TripRecord travelMessage) {
		this.travelMessage = travelMessage;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Date getSendTime() {
		return sendTime;
	}
	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}
}
