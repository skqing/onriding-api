package com.cloudwave.trailends.domain;

import com.cloudwave.base.domain.BaseEntity;

/**
 * @description 用户统计信息 实体类
 * @author DolphinBoy
 * @email dolphinboyo@gmail.com
 * @date 2013-12-21 下午10:41:39
 * TODO
 */

public class UserStatistics extends BaseEntity {
	private static final long serialVersionUID = 1L;

	private int trips;  //旅程
	private long tripRecords;  //行记
	private long mileage;  //里程
	private int favorites;  //收藏
	private int likes;  //喜欢
	private int wantToGo;  //想去
	private int follows;  //关注
	private int fans;  //粉丝
	
	private User user;  //属于user的统计信息
	
	public int getTrips() {
		return trips;
	}
	public void setTrips(int trips) {
		this.trips = trips;
	}
	public long getTripRecords() {
		return tripRecords;
	}
	public void setTripRecords(long tripRecords) {
		this.tripRecords = tripRecords;
	}
	public long getMileage() {
		return mileage;
	}
	public void setMileage(long mileage) {
		this.mileage = mileage;
	}
	public int getFavorites() {
		return favorites;
	}
	public void setFavorites(int favorites) {
		this.favorites = favorites;
	}
	public int getLikes() {
		return likes;
	}
	public void setLikes(int likes) {
		this.likes = likes;
	}
	public int getWantToGo() {
		return wantToGo;
	}
	public void setWantToGo(int wantToGo) {
		this.wantToGo = wantToGo;
	}
	public int getFollows() {
		return follows;
	}
	public void setFollows(int follows) {
		this.follows = follows;
	}
	public int getFans() {
		return fans;
	}
	public void setFans(int fans) {
		this.fans = fans;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
}