package com.cloudwave.trailends.web.interceptors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.cloudwave.base.action.AppAction;
import com.cloudwave.base.utils.SpringUtil;
import com.cloudwave.fwcore.utils.DateUtils;
import com.cloudwave.trailends.domain.UserSignLog;
import com.cloudwave.trailends.service.UserSignLogService;

/**
 * 参数拦截器 
 * @author 龙雪
 * @email 569141948@qq.com
 * @date 2014年12月15日
 */

public class ParamsInteceptor extends HandlerInterceptorAdapter {
	private final static Logger logger = LoggerFactory.getLogger(ParamsInteceptor.class);

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		logger.debug("ParamsInteceptor --> preHandle");
		HandlerMethod hm = (HandlerMethod) handler;
		
		String channelId = request.getParameter("channelId");
		String udid = request.getParameter("udid");
		String imei = request.getParameter("imei");
		String phone = request.getParameter("phone");
		
		AppAction bc = (AppAction) hm.getBean();
		UserSignLog signLog = new UserSignLog();
		signLog.setCid(channelId);
		signLog.setUserId(bc.getUserId());
		signLog.setImei(imei);
		signLog.setUdid(udid);
		signLog.setPhone(phone);
		signLog.setPlatform("MOBILE");
		signLog.setCreateTime(DateUtils.now());
		bc.setUserSignLog(signLog);
		return super.preHandle(request, response, handler);
	}
	
	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		HandlerMethod hm = (HandlerMethod) handler;
		AppAction bc = (AppAction) hm.getBean();
		UserSignLog userSignLog = bc.getUserSignLog();
		UserSignLogService userSignLogService = (UserSignLogService) SpringUtil.getObject(UserSignLogService.class);
		userSignLogService.save(userSignLog);
		
		logger.debug("ParamsInteceptor --> afterCompletion");
	}
	
}
