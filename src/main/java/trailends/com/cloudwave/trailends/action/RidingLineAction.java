package com.cloudwave.trailends.action;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.cloudwave.base.action.AppAction;
import com.cloudwave.base.entity.ResultEntity;
import com.cloudwave.fwcore.entity.Result;
import com.cloudwave.trailends.domain.RidingLine;
import com.cloudwave.trailends.domain.UserSignLog;
import com.cloudwave.trailends.service.RidingLineService;

/**
 * 
 * @author 龙雪
 * @email 569141948@qq.com
 * @date 2014-12-18 下午10:17:33
 * 
 */

@Controller
@RequestMapping("/api/ridingline")
public class RidingLineAction extends AppAction {
	private static Logger logger = LoggerFactory.getLogger(RidingLineAction.class);
	
	
	@Resource
	private RidingLineService ridingLineService;
	
	
	@RequestMapping(method=RequestMethod.POST, value="/update")
	public @ResponseBody ResultEntity update(HttpServletRequest request
			, @RequestParam(value="entity") String entity) {
		super.getUserSignLog().setAction(UserSignLog.ACTION_RIDINGLINE_UPDATE);
		
		RidingLine rl = JSON.parseObject(entity, RidingLine.class);
		
		if ( rl == null ) {
			return resultEntity.failure(Result.UNKNOW_USER);
		}
		
		RidingLine rlTmp = ridingLineService.findById(rl.getId());
		
		if ( rlTmp == null ) {
			return resultEntity.failure(Result.UNKNOW_DATA);
		}
		
		rlTmp.setTitle(rl.getTitle());
		rlTmp.setDescription(rl.getDescription());
		
		try {
			ridingLineService.update(rlTmp);
			return resultEntity.success();
		} catch (Exception e) {
			logger.error("update ridingline error!", e);
			return resultEntity.failure(Result.UPDATE_FAILURE);
		}
	}
	
}
