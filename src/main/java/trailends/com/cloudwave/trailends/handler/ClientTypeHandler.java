package com.cloudwave.trailends.handler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

import com.cloudwave.trailends.emuns.ClientType;

/**
 * @description 
 * @author DolphinBoy
 * @date 2013年12月19日
 * TODO
 */

public class ClientTypeHandler implements TypeHandler<ClientType> {

	@Override
	public ClientType getResult(ResultSet rs, String columnName) throws SQLException {
		int v = rs.getInt(columnName);
		return ClientType.getById(v);
	}
	@Override
	public ClientType getResult(ResultSet rs, int columnIndex) throws SQLException {
		int v = rs.getInt(columnIndex);
		return ClientType.getById(v);
	}
	@Override
	public ClientType getResult(CallableStatement rs, int columnIndex) throws SQLException {
		 return ClientType.getById(columnIndex);
	}
	@Override
	public void setParameter(PreparedStatement ps, int i, ClientType parameter,
			JdbcType jdbcType) throws SQLException {
		ps.setInt(i, parameter.getId());
	}
}
