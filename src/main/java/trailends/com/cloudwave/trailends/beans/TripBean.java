package com.cloudwave.trailends.beans;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * @description 
 * @author DolphinBoy
 * @email dolphinboyo@gmail.com
 * @date 2013-10-17 下午10:40:06
 * TODO
 */

public class TripBean extends BaseBean {
	private static final long serialVersionUID = 1L;
	
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Date beginDate;
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Date endDate;
	
	private String beginLocation;
	private String endLocation;
	
	private Long userId;
	
	private Long partner;

	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getBeginLocation() {
		return beginLocation;
	}

	public void setBeginLocation(String beginLocation) {
		this.beginLocation = beginLocation;
	}

	public String getEndLocation() {
		return endLocation;
	}

	public void setEndLocation(String endLocation) {
		this.endLocation = endLocation;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getPartner() {
		return partner;
	}

	public void setPartner(Long partner) {
		this.partner = partner;
	}
	
	
//	public ResultEntity toTravels() {
////		ResultEntity re = new ResultEntity(ResultEntity.FAIL);
//		Trip t = new Trip();
////		t.setText(this.getText());
//		
//		
//		return re;
//	}
}
