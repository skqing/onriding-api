TrailEnds_Service
==============

TrailEnds是[车轮不息](http://www.trailends.cc)服务端程序,主要用户和移动客户端交互.

框架: spring + springmvc + mybatis

管理工具: Maven

版本控制工具: Git





模版引擎:[httl](http://httl.github.io/zh/)  
前台插件:[Bootstrap插件](http://www.oschina.net/news/43645/30-amazing-plugins-extend-twitter-bootstrap?from=20130901)  



参考资料：  
http://www.ideawu.net/person/spring_mvc.html  
http://sishuok.com/forum/blogPost/list/5160.html

http://www.zhurouyoudu.com/index.php/archives/801/

https://github.com/SpringSource
